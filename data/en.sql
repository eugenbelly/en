-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 08 2015 г., 13:15
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `en`
--

-- --------------------------------------------------------

--
-- Структура таблицы `added`
--

CREATE TABLE IF NOT EXISTS `added` (
  `user_login` varchar(30) NOT NULL,
  `article_id` int(11) NOT NULL,
  `sentence` int(11) NOT NULL,
  PRIMARY KEY (`user_login`,`article_id`,`sentence`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `added`
--

INSERT INTO `added` (`user_login`, `article_id`, `sentence`) VALUES
('qwert', 1, 1),
('qwert', 2, 0),
('qwert', 2, 1),
('qwert', 2, 2),
('qwert', 2, 3),
('qwert', 2, 5),
('qwert', 6, 2),
('qwert', 6, 3),
('qwert', 6, 4),
('qwert', 6, 5),
('qwert', 6, 7),
('qwert', 6, 10),
('sdfs', 3, 0),
('sdfs', 3, 1),
('sdfs', 5, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `name`, `text`, `published`) VALUES
(2, 'Accidents', '["I wonder how many accidents I’ve had in my life.", "I’ve had a few serious ones where I’ve ended up in hospital.", "Traffic accidents are the worst.", "They’re always painful.", "I haven’t had too many work-related accidents.", "I suppose that’s because I do office work and that’s not so dangerous.", "Most of my accidents are those around the house.", "I’ve lost count of the number of times I’ve hit my thumb with a hammer.", "I’m also really good at standing up and hitting my head on something.", "I’m not as bad as my friend though.", "He’s a real accident looking for somewhere to happen.", "Almost every time we meet, he has some story to tell about his latest accident.", "He’s quite unbelievable.", "I think I’d be very worried if I were his mother."]', 1),
(3, 'Actors', '["Being an actor must be great.", "I really wish I could act.", "I’d love to have the confidence to act in front of people.", "Being a member of the audience at the theatre is great,", "but acting on stage is better.", "I wonder what it’s like to be a movie actor.", "I think you need to be beautiful or handsome to be a successful Hollywood or Bollywood actor.", "They all look great.", "Actors are very lucky.", "They get paid huge amounts of money to do what they love doing.", "My favourite actors are all dead now.", "I really like the actors in the old black and white movies.", "They all looked so cool.", "Actors in the old days seemed to be more glamorous than today’s actors.", "The best thing about being an actor is that all of your friends are famous too."]', 1),
(4, 'Advertising', '["What is advertising?", "Is it telling the truth or is it making things look better than they really are?", "Or is it lying?", "Companies pay a lot of money for adverts.", "Some of the ads you see in glossy magazines look like art.", "The commercials on TV look like mini movies.", "Do they really change our behaviour?", "Do adverts make you buy things?", "I think some advertising is a form of lying.", "Is BMW really “The ultimate driving machine” like they say in their ads?", "British Airways used to say they were “The world’s favourite airline,” but had to stop saying it because it wasn’t true.", "Personally, I get tired of watching ads on television.", "They always interrupt a good programme.", "I like ads in magazines.", "They’re usually quite interesting."]', 0),
(5, 'Advice', '["I don’t know what I would do without my friends’ advice.", "They’ve all given me so much good advice over the years.", "It’s sometimes very difficult to make decisions on your own.", "Listening to advice can be great for helping you make the right decision.", "The greatest piece of advice I’ve received was from my father.", "He told me I can do anything in life if I try hard.", "He was right.", "I am now passing on his advice to my own children.", "Right now I need some advice on money.", "My financial advisors are telling me to be very careful.", "That’s very sound advice.", "I would advise anyone to do the same.", "Someone asked me the other day about the worst piece of advice I’ve ever had.", "I couldn’t answer.", "I don’t think I’ve had any really bad advice."]', 1),
(6, 'Airplanes', '["Airplanes are amazing.", "How does something so big and heavy get off the ground?", "I’m always amazed at how the millions of different parts work together.", "Travelling by airplane is always a wonderful experience.", "I don’t care whether economy class is cramped and has no space.", "I like playing with the in-flight entertainment system,", "especially now they have all the latest movies.", "I also love airplane food.", "Many of my friends say it’s disgusting, but I love it.", "I often ask the passenger next to me if I can have the dessert or roll they don’t want.", "The only thing I don’t like about planes is turbulence.", "When the airplane hits those air pockets, I always worry we’ll crash.", "But I once read that turbulence has never caused an airplane to crash."]', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `login` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`login`, `pass`) VALUES
('qwert', 'qwert');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
