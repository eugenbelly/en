var ViewModel = function (){
    var self = this;

    //Constants

    self.AUDIO_FILES_PATH = 'data/audio/';
    self.PHP_SCRIPTS_PATH = 'php/ajax/';
    self.fragmentAudioIdSuffix = 'sentenceAudio_';

    //Common
    self.info = ko.observable('get article');

    self.audioFileName = ko.observable();

    self.sentences = ko.observableArray();

    self.currentFragment = ko.observable(0);

    self.runScript = function (scriptName, inputData, callback) {

        $.ajax({
            url: self.PHP_SCRIPTS_PATH + scriptName + '.php',
            method: 'GET',
            data: inputData,
            success: function (resp) {
                console.log('resp', resp);
                var data = JSON.parse(resp);
                callback(data);
            }
        });
    }

    self.currentSentenceText = ko.computed(function(){
        if(self.sentences().length > 0) {
            var obj = self.sentences()[self.currentFragment()];
            return obj.text;
        }
    });

    self.audioData = ko.computed(function () {
        var data = [];
        $.each(self.sentences(), function (k, sentence) {
            data.push({
                id: self.fragmentAudioIdSuffix + sentence.key,
                src: self.AUDIO_FILES_PATH + self.audioFileName() + '/' + self.audioFileName() + '_' + sentence.key + '.mp3'
            });
        });
        return data;
    });

    self.incrementFragment = function(){
        if(!self.currentFragmentIsLast()) {
            self.currentFragment(self.currentFragment() + 1);
        }
    };

    self.decrementFragment = function(){
        if(!self.currentFragmentIsFirst()) {
            self.currentFragment(self.currentFragment() - 1);
        }
    };

    self.currentFragmentIsFirst = ko.computed(function(){
        if(self.currentFragment() > 0) {
            return false;

        } else {
            return true;

        }
    });

    self.currentFragmentIsLast = ko.computed(function(){
        if(self.currentFragment() < self.sentences().length - 1) {
            return false;

        } else {
            return true;

        }
    });

    self.currentFragmentIsAdded = ko.computed(function(){
        if(self.sentences().length > 0) {
            return self.isSentenceAdded(self.currentFragment());
        }

    });

    self.dataIsLoading = ko.observable(false);

    self.article_id = ko.observable();

    self.loadArticle = function (id) {

        if(self.isPlayingNow()){
            self.stopPlaying();
        }

        return function () {

            self.article_id(id);
            self.dataIsLoading(true);
            self.info('Loading');
            self.runScript('get_article', {id: id}, function (data) {
                self.audioFileName(data.audioFileName);
                self.sentences(data.sentences);
                self.addedSentences(data.added)

                self.setAudioListeners();
                self.currentFragment(0);
                self.dataIsLoading(false);

                self.playCurrentFragment();
                console.log("addedSentences", self.addedSentences());
                console.log("rightPanelSentences", self.rightPanelSentences());
            });
        }
    };

    //Player functions

    self.isPlayingNow = ko.observable(false);
    self.playCurrentFragment = function(){
        document.getElementById(self.fragmentAudioIdSuffix + self.currentFragment()).play();
        console.log(self.currentFragment()+" is playing");
        self.isPlayingNow(true);
    };
    self.stopPlaying = function(){
        var audio = document.getElementById(self.fragmentAudioIdSuffix + self.currentFragment());
        audio.pause();
        audio.currentTime = 0;
        console.log(self.currentFragment()+" is stopped");
        self.isPlayingNow(false);
    };

    self.playbackButtonClick = function(){
        if(self.isPlayingNow()){
            self.stopPlaying();

        } else {
            self.playCurrentFragment();

        }
    };

    self.nextSentenceButtonClick = function(){
        self.stopPlaying();
        if(!self.currentFragmentIsLast()) {
            self.incrementFragment();

        } else {
            self.currentFragment(0);
        }
        self.playCurrentFragment();
    };
    self.prevSentenceButtonClick = function(){
        self.stopPlaying();
        if(!self.currentFragmentIsFirst()) {
            self.decrementFragment();
        } else {
            self.currentFragment(self.sentences().length - 1);
        }
        self.playCurrentFragment();
    };
    self.playFragment = function(num){
        return function(){
            self.stopPlaying();
            self.currentFragment(num);
            self.playCurrentFragment();
        }
    };

    self.setAudioListeners = function(){
        $.each(self.sentences(), function (k, sentence) {
            $('#' + self.fragmentAudioIdSuffix + sentence.key).on('ended', function () {
                self.stopPlaying();
                if(self.playNonstop() && !self.currentFragmentIsLast()){
                    self.incrementFragment();
                    self.playCurrentFragment();
                }
            });
        });
    };

    // UI
    self.playNonstop = ko.observable(true);

    self.showText = ko.observable(true);

    self.toggle = function(observable){
        return function () {
            if(typeof observable() == 'boolean') {
                if (observable() == true) {
                    observable(false);
                } else {
                    observable(true);
                }
            }
        }
    }

    self.allArticles = ko.observableArray([]);

    self.getAllArticles = function () {
        self.runScript('getAllArticles', {}, function (data) {
            for( var i in data) {
                self.allArticles.push(data[i]);
            }
        });
    }

    self.addButtonClick = function () {
        var action;

        if(self.currentFragmentIsAdded()) {
            var key = self.addedSentences().indexOf(self.currentFragment());
            self.addedSentences.splice(key, 1);
            action = 'remove';

            var arr = self.rightPanelSentences();
            for(var i = 0; i < arr.length; i++){
                if (
                    parseInt(arr[i].filename[arr[i].filename.length - 1]) == self.currentFragment() &&
                    arr[i].name == self.audioFileName()
                ){
                    self.rightPanelSentences.splice(i, 1);
                }
            }

        } else {
            self.addedSentences.push(self.currentFragment());
            action = 'add';

            self.rightPanelSentences.push({
                filename: self.audioFileName() + "_" + self.currentFragment(),
                name: self.audioFileName(),
                text: self.sentences()[self.currentFragment()].text,
                filepath:
                    self.AUDIO_FILES_PATH +
                    self.audioFileName() + "/" +
                    self.audioFileName() + "_" +
                    self.currentFragment() + ".mp3"
            });
        }

        self.runScript('add', {
            article_id: self.article_id(),
            sentence_num: self.currentFragment(),
            action: action
        }, function (data) {
            console.log(data);
        });
    }

    self.addedSentences = ko.observableArray([]);

    self.isSentenceAdded = function (num) {
        if (self.addedSentences().indexOf(num) == -1){
            return false;
        } else {
            return true;
        }
    };

    self.rightMenuIsOpened = ko.observable(false);

    self.toggleRightMenu = function () {
        self.toggle(self.rightMenuIsOpened)();
    }

    self.rightPanelSentences = ko.observableArray([]);

    self.prevAddedSentence = false;

    self.rightPanelPlay = function (filename) {
        return function () {
            if(self.prevAddedSentence) {
                self.prevAddedSentence.pause();
                self.prevAddedSentence.currentTime = 0;
            }
            self.prevAddedSentence = document.getElementById(filename);
            self.prevAddedSentence.play();
        }
    }

    self.getAllArticles();

    self.runScript('getAddedOfUser', {}, function (data) {
        self.rightPanelSentences(data);
    });

    self.authError = ko.observable(false);
    self.login = ko.observable('');
    self.pass = ko.observable('');
    self.authorized = ko.observable(false);
    self.hello = ko.computed(function () {
        return 'Hello, '+ self.login();
    });
    self.authorize = function () {

        var data = {
            login: self.login,
            pass: self.pass
        }

        self.runScript('auth', data, function (data) {
            if(!data.error){
                document.cookie = 'en_login='+self.login();
                self.authorized(true);
                self.runScript('getAddedOfUser', {}, function (data) {
                    self.rightPanelSentences(data);
                });
                self.loadArticle(self.article_id())();
            } else {
                self.authError(true);
            }
        })
    }

    self.isAuthorised = function () {
        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i=0; i<ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1);
                if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
            }
            return "";
        }
        var username = getCookie('en_login');
        if(username !== ""){
            self.login(username);
            self.authorized(true);
        } else {
            self.login('');
            self.pass('');
            self.authorized(false);
        }
    }

    self.logOut = function () {
        function setCookie(name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires == "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }

            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;
        }

        setCookie('en_login', "", {});
        self.runScript('getAddedOfUser', {}, function (data) {
            self.rightPanelSentences(data);
        });
        self.loadArticle(self.article_id())();

        self.isAuthorised();
    }

    self.isAuthorised();
};

ko.applyBindings(new ViewModel());