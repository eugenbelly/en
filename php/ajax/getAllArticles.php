<?php require_once('../../config.php');

$res = array();
foreach($db->query('SELECT * FROM article WHERE published != 0') as $row) {

    $text = '';
    foreach(json_decode($row['text']) as $sentence){
        $text .= $sentence." ";
    }

    $article['id'] = $row['id'];
    $article['name'] = $row['name'];
    $article['text'] = $text;
    $article['published'] = $row['published'] == '0' ? false : true;
    $res[] = $article;
}

echo json_encode($res);