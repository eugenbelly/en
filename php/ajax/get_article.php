<?php require_once('../../config.php');

$article = null;

foreach($db->query('SELECT * FROM article WHERE published != 0 AND id = '.$_REQUEST['id'].' ORDER BY text ASC') as $row) {

    $sentences_text = json_decode($row['text']);
    $sentences = array();
    for($i = 0; $i < count($sentences_text); $i++){
        $sentences[$i]["key"] = $i;
        $sentences[$i]['text'] = $sentences_text[$i];
    }

    $article['audioFileName'] = strtolower(str_replace(' ', '_', $row['name']));
    $article['sentences'] = $sentences;

}

$added = array();
foreach($db->query('SELECT sentence FROM added WHERE user_login = "'.$_COOKIE['en_login'].'" AND article_id = '.$_REQUEST['id']) as $row) {
    $added[] = intval($row['sentence']);
}

$article['added'] = $added;

echo json_encode($article);