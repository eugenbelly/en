<?php
$intervals = $_REQUEST['intervals'];
$filename = $_REQUEST['filename'];
$path = 'C:\\WebServers\\home\\en\\www\\data\\audio\\'.$filename.'\\';

$commands = array();
for($i = 0; $i < count($intervals) - 1; $i++){
    $startTime = $intervals[$i];
    $duration = $intervals[$i+1] - $intervals[$i];

    $command = "ffmpeg
        -i ${path}${filename}.mp3
        -acodec copy
        -ss $startTime
        -t $duration
        ${path}${filename}_${i}.mp3
    ";
    $commands[] = preg_replace('/([\n\r\t\s])+/', ' ', $command);
}

$answer = array();
foreach($commands as $command){
    $answer[] = exec($command);
};

echo json_encode($commands);