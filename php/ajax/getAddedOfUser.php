<?php require_once('../../config.php');

$res = array();
foreach($db->query('
    SELECT `text`, `name`, `sentence`
    FROM added
    JOIN article
    ON article.id = article_id
    WHERE user_login = "'.$_COOKIE['en_login'].'"
') as $row) {
    $text = json_decode($row['text']);
    $sentenceText = $text[$row['sentence']];
    $articleName = $row['name'];
    $filename = strtolower(str_replace(' ', '_', $articleName));
    $res[] = array(
        'text' => $sentenceText,
        'name' => $articleName,
        'filepath' => "data/audio/$filename/$filename"."_".$row['sentence'].".mp3",
        'filename' => $filename."_".$row['sentence']
    );
}

echo json_encode($res);