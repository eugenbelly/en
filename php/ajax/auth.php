<?php
require_once('../../config.php');

if(isset($_REQUEST['login']) && $_REQUEST['pass']) {

    $login = $_REQUEST['login'];
    $pass = $_REQUEST['pass'];

    $userIsExistSQL = $db->query("
        SELECT COUNT(*)
        FROM user
        WHERE login = '{$login}'
    ");

    $getPassFromDbSQL = $db->query("
        SELECT pass
        FROM user
        WHERE login = '{$login}'
    ");

    $registerSQL = $db->query("
        INSERT INTO user (login, pass)
        VALUES('{$login}', '{$pass}')
    ");

    $sqlResponse = array();
    foreach($userIsExistSQL as $resp){
        $sqlResponse[] = $resp;
    };

    $userIsExist = intval($sqlResponse[0][0]);

    if ($userIsExist > 0) {

        $sqlResponse = array();
        foreach($getPassFromDbSQL as $resp){
            $sqlResponse[] = $resp;
        };

        $dbPass = $sqlResponse[0][0];

        if ($dbPass === $pass) {
            echo '{
                "status": "logined",
                "error": false,
                "login": "'.$login.'"
            }';

        } else {
            echo '{
                "status": "incorrect_pass",
                "error": true
            }';
        }

    } else {
        $registerSQL->execute();
        echo '{
            "status": "registered",
            "error": false,
            "login": "'.$login.'"
        }';
    }
}
//else {
//    print_r($_REQUEST);
//}