<script type="text/javascript" src="//code.jquery.com/jquery-2.1.3.min.js"></script>

<canvas width="9600" id="canvas"></canvas>

<style>
    canvas {
        image-rendering: pixelated;
        height: 200px;
    }

</style>

<script type="text/javascript">

function drawWave() {
    function drawBuffer(width, height, context, buffer) {
        var data = buffer.getChannelData(0);
        var step = Math.ceil(data.length / width);
        var amp = height / 2;
        for (var i = 0; i < width; i++) {
            var min = 1.0;
            var max = -1.0;
            for (var j = 0; j < step; j++) {
                var datum = data[(i * step) + j];
                if (datum < min)
                    min = datum;
                if (datum > max)
                    max = datum;
            }
            context.fillRect(i, (1 + min) * amp, 1, Math.max(1, (max - min) * amp));
        }
    }

    function loadSound(url, callback) {
        var request = new XMLHttpRequest();
        request.open("GET", url, true);
        request.responseType = "arraybuffer";

        request.onload = function () {
            callback(request);
        };

        request.send();
    }

    function processData(request) {
        var audioContext = new AudioContext();
        audioContext.decodeAudioData(request.response, function (buffer) {
            var canvas = document.getElementById("canvas");
            drawBuffer(canvas.width, canvas.height, canvas.getContext('2d'), buffer);
        });
    }

    loadSound("/data/audio/one/one.mp3", processData);
}
</script>