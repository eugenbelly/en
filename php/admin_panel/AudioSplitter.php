<?php

class AudioSplitter{

    private $splitSections;
    private $fileName;
    private $inputPath;
    private $outputPath;

    public function __construct($fileName, $splitSections, $outputPath = null, $inputPath = null){
        $this->splitSections = $splitSections;
        $this->fileName = $fileName;
        $this->inputPath = $inputPath;
        $this->outputPath = $outputPath;
    }

    public function cropByte($startTime, $endTime, $counter){
        $fileName = $this->fileName;
        $inputPath = $this->inputPath;
        $outputPath = $this->outputPath;

        if($outputPath){
            exec("mkdir $outputPath");
            $outputPath .= '/';
        }

        if($inputPath){
            $inputPath .= '/';
        }

        echo "$inputPath --- $outputPath";
        $cropCommand =
            "ffmpeg -i ".
            "${inputPath}${fileName}.mp3 ".
            "-acodec copy -ss ".
            "${startTime} -t ${endTime} ".
            "${outputPath}${fileName}_${counter}.mp3";

        echo $cropCommand."<br>";
        echo exec($cropCommand)."<br><br>";
    }

    public function execute(){
        $counter = 1;
        foreach($this->splitSections as $section){
            $this->cropByte($section[0], $section[1], $counter++);
        }
    }
}

$sections = array(
    array('00:00:00', '00:00:02'),
    array('00:00:02', '00:00:04'),
    array('00:00:04', '00:00:06'),
    array('00:00:06', '00:00:08'),
    array('00:00:08', '00:00:10'),
    array('00:00:10', '00:00:12'),
);
$input = 'C:\\WebServers\\home\\en\\www\\data\\audio\\one\\';
$splitter = new AudioSplitter('one', $sections, $input, $input);
$splitter->execute();