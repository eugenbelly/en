<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<canvas id="canvas" width="1000" height="700"></canvas>
<script src="/app/js/libs/ocanvas.js"></script>
<script>
    var canvas = oCanvas.create({
        canvas: "#canvas"
    });

    var rectangle = canvas.display.rectangle({
        x: 0,
        y: 0,
        width: 1000,
        height: 250,
        fill: "#AAA"
    });



    var addSplitterLine = function(){

        var canvasHeight = 250;

        var VerticalLine = function (x) {
            this.x = x;
            this.getOcanvasObject = function () {
                return canvas.display.line({
                    start: { x: x, y: 0 },
                    end: { x: x, y: canvasHeight },
                    stroke: "2px #000"
                })
            }
        }

        var lines = [
            new VerticalLine(100).getOcanvasObject(),
            new VerticalLine(200).getOcanvasObject()
        ];

        $.each(lines, function (i, l) {
            canvas.addChild(lines[i]);
        });

        rectangle.bind("mousedown", function () {

            var line = null;
            var min_x = Infinity;
            $.each(lines, function (i, l) {
                if(Math.abs(lines[i].abs_x - canvas.mouse.x) < min_x){
                    min_x = lines[i].abs_x;
                    line = lines[i];
                }
            });

            line.moveTo(canvas.mouse.x, 125);
            canvas.redraw();

            function mousemoveHandler() {
                line.moveTo(canvas.mouse.x, 125);
                canvas.redraw();
            };

            rectangle.bind("mousemove", mousemoveHandler);

            line.bind("mouseup", function () {
                rectangle.unbind("mousemove", mousemoveHandler);
            });

            rectangle.bind("mouseup", function () {
                rectangle.unbind("mousemove", mousemoveHandler);
            });
        });



    };

    canvas.addChild(rectangle);
</script>