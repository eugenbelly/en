<link href="app/css/style.css" type="text/css"/>
<audio id="player"/></audio>
<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<div id="canvasWrapper">
    <canvas id="waveCanvas"></canvas>
    <canvas id="canvas" style="margin-top: -250px;"></canvas>
</div>

<script src="/app/js/libs/ocanvas.js"></script>
<script>
    var filename = location.search.split('filename=')[1];
    player.src = "/data/audio/"+filename+"/"+filename+".mp3";
    var WavePanel = function (canvasId, canvasHeight, canvasWidth, player) {

        var WP = this;

        WP.canvasId = canvasId;

        WP.trackTimeInSeconds = parseInt(player.duration);

        WP.canvas = oCanvas.create({
            canvas: "#" + WP.canvasId
        });

        WP.canvas.height = canvasHeight;
        WP.canvas.width = canvasWidth;

        WP.background = WP.canvas.display.rectangle({
            x: 0,
            y: 0,
            width: WP.canvas.width,
            height: WP.canvas.height
        });

        WP.canvas.addChild(WP.background);

        var VerticalLine = function (x, color) {
            var VL = this;

            VL.x = x;

            VL.color = color || '#000';

            VL.ocanvasObject = WP.canvas.display.line({
                start: {x: VL.x, y: 0},
                end: {x: VL.x, y: WP.canvas.height},
                stroke: "2px " + VL.color
            });

            VL.move = function (time) {
                var newX = WP.pixelsToSeconds(time, 'revert');
                VL.x = newX;
                VL.ocanvasObject.moveTo(newX, WP.canvas.height/2);
                WP.canvas.redraw();
            }
        }

        WP.lines = {
            progressLine: new VerticalLine(0, '#F00'),
            splitters: []
        };

        WP.pixelsToSeconds = function (val, revert) {
            var revert = revert || false;
            if(!revert){
                return WP.trackTimeInSeconds * val / WP.canvas.width;

            } else {
                return parseInt(WP.canvas.width * val / WP.trackTimeInSeconds);
            }
        }

        WP.getTimeArray = function () {
            var times = [];
            $.each(WP.lines.splitters, function (i, l) {
                times.push(WP.pixelsToSeconds(WP.lines.splitters[i].x));
            });
            times.push(player.duration);
            return times.sort(function(a, b){
                return (a - b);
            });
        }

        WP.getClosestSplitterLine = function () {
            var closestSplitterLine = null;
            var min_x = Infinity;
            var num;

            function distanceToSplitter(i){
                return Math.abs(WP.lines.splitters[i].x - WP.canvas.mouse.x);
            }

            $.each(WP.lines.splitters, function (i, l) {
                if(distanceToSplitter(i) < min_x){
                    min_x = distanceToSplitter(i);
                    num = i;
                    closestSplitterLine = WP.lines.splitters[i];
                }
            });

            return closestSplitterLine;
        }

        WP.addSplitter = function (x) {
            var splitter = new VerticalLine(x);
            WP.lines.splitters.push(splitter);
            WP.canvas.addChild(splitter.ocanvasObject);
        }

        WP.setSplitters = function(coordsArray){
            $.each(coordsArray, function (i, l) {
                WP.addSplitter(WP.pixelsToSeconds(coordsArray[i], 'revert'));
            });
        };

        WP.dragNdropBehaviour = function(){
            var splitterLine = WP.getClosestSplitterLine();

            function mousemoveHandler() {
                splitterLine.x = WP.canvas.mouse.x;
                splitterLine.ocanvasObject.moveTo(WP.canvas.mouse.x, WP.canvas.height/2);
                WP.canvas.redraw();
            };

            mousemoveHandler();

            WP.background.bind("mousemove", mousemoveHandler);

            splitterLine.ocanvasObject.bind("mouseup", function () {
                WP.background.unbind("mousemove", mousemoveHandler);
            });

            WP.background.bind("mouseup", function () {
                WP.background.unbind("mousemove", mousemoveHandler);
            });
        };

        WP.addingBehaviour = function(){
            WP.addSplitter(WP.canvas.mouse.x);
            WP.dragNdropBehaviour();
        }

        WP.playbackBehaviour = function () {
            var splitterLine = WP.lines.progressLine;

            function mousemoveHandler() {
                player.currentTime = WP.pixelsToSeconds(parseInt(WP.canvas.mouse.x));
                splitterLine.move(WP.canvas.mouse.x);
                WP.canvas.redraw();
            };

            mousemoveHandler();

            WP.background.bind("mousemove", mousemoveHandler);

            splitterLine.ocanvasObject.bind("mouseup", function () {
                WP.background.unbind("mousemove", mousemoveHandler);
            });

            WP.background.bind("mouseup", function () {
                WP.background.unbind("mousemove", mousemoveHandler);
            });
        }

        WP.addLineAtTheTime = function () {
            WP.addSplitter(WP.pixelsToSeconds(player.currentTime, 'revert'));
        }

        WP.currentBehaviour = WP.dragNdropBehaviour;

        WP.background.bind("mousedown", function () {
            WP.currentBehaviour();
        });

        WP.canvas.addChild(WP.lines.progressLine.ocanvasObject);

        WP.drawWave = function() {
            function drawBuffer(width, height, context, buffer) {
                var data = buffer.getChannelData(0);
                var step = Math.ceil(data.length / width);
                var amp = height / 2;
                for (var i = 0; i < width; i++) {
                    var min = 1.0;
                    var max = -1.0;
                    for (var j = 0; j < step; j++) {
                        var datum = data[(i * step) + j];
                        if (datum < min)
                            min = datum;
                        if (datum > max)
                            max = datum;
                    }
                    context.fillStyle="#555";
                    context.fillRect(i, (1 + min) * amp, 1, Math.max(1, (max - min) * amp));
                }
            }

            function loadSound(url, callback) {

                document.getElementById('waveCanvas').height = canvasHeight;
                document.getElementById('waveCanvas').width = canvasWidth;

                var request = new XMLHttpRequest();
                request.open("GET", url, true);
                request.responseType = "arraybuffer";

                request.onload = function () {
                    callback(request);
                };

                request.send();
            }

            function processData(request) {
                var audioContext = new AudioContext();
                audioContext.decodeAudioData(request.response, function (buffer) {
                    var canvas = document.getElementById("waveCanvas");
                    drawBuffer(canvas.width, canvas.height, canvas.getContext('2d'), buffer);
                });
            }

            loadSound(player.src, processData);

        }

    };

    var WP;
    player.addEventListener('loadeddata', function() {
        WP = new WavePanel('canvas', 250, 4096, player);

        setInterval(function () {
            WP.lines.progressLine.move(player.currentTime);
        }, 10);

        WP.drawWave();

    }, false);



    function playback(){
        if(player.paused) {
            player.play();
        } else {
            player.pause();
        }
    }

    function save(){
        if(confirm('Are you sure?')) {
            console.log(WP.getTimeArray());
            $.ajax({
                url: 'php/ajax/saveArticle.php',
                method: 'GET',
                data: {
                    intervals: WP.getTimeArray(),
                    filename: location.search.split('filename=')[1]
                },
                success: function (resp) {
                    var data = JSON.parse(resp);
                    console.log(data);
                }
            });
        }
    }

</script>
<br>
<button onclick="WP.currentBehaviour = WP.dragNdropBehaviour;">Editing</button>
<button onclick="WP.currentBehaviour = WP.addingBehaviour;">Adding</button>
<button onclick="WP.addLineAtTheTime();">ADD NOW</button>
<button onclick="WP.currentBehaviour = WP.playbackBehaviour;">Playback</button>
<button onclick="playback()">Play</button>
<br><br><br>
<button onclick="save()">Save</button>