<html>
    <head>
        <link rel="stylesheet" href="app/css/style.css"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
        <script src="app/js/libs/ko.js"></script>
        <script src="app/js/libs/jQ.js"></script>
    </head>
    <body>
        <div class="authorization" data-bind="visible: !$root.authorized()">
            <div class="form">
                <input placeholder="login" class="login" data-bind="
                    value: $root.login
                "/>
                <input placeholder="password" class="pass" data-bind="
                    value: $root.pass
                "/>
            </div>
            <div class="incorrect" data-bind="
                visible: $root.authError()
            ">
                Incorrect password
            </div>
            <div class="button" data-bind="
                click: $root.authorize
            ">
                Authorize
            </div>
        </div>
        <div class="authorization" data-bind="visible: $root.authorized()">
            <div data-bind="text: $root.hello"></div>
            <div class="button" data-bind="click: $root.logOut">
                Log Out
            </div>
        </div>
        <div class="articles" data-bind="foreach: allArticles">
            <div class="article current" data-bind="
                attr:{
                    id: 'art_' + $data.name
                },
                css: {
                    current: $root.audioFileName() == $data.name
                }
            ">
                <a class="name" data-bind="
                    text: $data.name,
                    click: $root.loadArticle($data.id)
                    " href="#">
                </a>
                <div class="playNote">(play)</div>
                <div class="text" data-bind="text: $data.text"></div>
            </div>
        </div>

        <div class="audioWrapper" data-bind="foreach: $root.audioData">
            <audio data-bind="
                attr: {
                    id: $data.id,
                    src: $data.src
                }">
            </audio>
        </div>

        <div id="player" data-bind="visible: $root.sentences().length > 0" style="display: none;">
            <div id="sentenceText" data-bind="
                visible: $root.showText(),
                text: currentSentenceText">
            </div>

            <table id="addedBar">
                <tr data-bind="foreach: $root.sentences()">
                    <td>
                        <div data-bind="
                            css:{
                                isAdded: $root.isSentenceAdded($data.key)
                            }">
                        </div>
                    </td>
                </tr>
            </table>

            <table id="progressBar">
                <tr data-bind="foreach: $root.sentences()">
                    <td data-bind="
                        click: $root.playFragment($data.key),
                        attr:{
                            id: 'frag_'+$data.key
                        }
                    ">
                        <div data-bind="
                            css:{
                                current: $root.currentFragment() == $data.key
                            }">
                        </div>
                    </td>
                </tr>
            </table>

            <table id="blaybackPannel"><tr>
                <td></td>
                <td>
                    <div>
                        <a href="#" class="player-btn-prev" data-bind="
                            click: $root.prevSentenceButtonClick">
                        </a>
                        <a href="#" data-bind="
                            click: $root.playbackButtonClick,
                            css: {
                                'player-btn-play': !$root.isPlayingNow(),
                                'player-btn-stop': $root.isPlayingNow()
                            }">
                        </a>
                        <a href="#" class="player-btn-next" data-bind="
                            click: $root.nextSentenceButtonClick">
                        </a>
                        <a href="#" data-bind="
                            click: $root.addButtonClick,
                            css: {
                                'player-btn-add': !$root.currentFragmentIsAdded(),
                                'player-btn-remove': $root.currentFragmentIsAdded()
                            },
                            visible: $root.authorized
                            ">
                        </a>
                    </div>
                </td>
                <td>
                    <a href="#" class="textBtn" data-bind="
                        click: $root.toggle($root.playNonstop),
                    ">
                        <!-- ko if: $root.playNonstop() -->
                            Play Single Sentence
                        <!-- /ko --><!-- ko ifnot: $root.playNonstop() -->
                            Play Nonstop
                        <!-- /ko -->
                    </a>

                    <a href="#" class="textBtn" data-bind="
                        css: {inactive: !$root.showText()},
                        click: $root.toggle($root.showText)
                    ">
                        <!-- ko if: $root.showText() -->
                            Hide Text
                        <!-- /ko --><!-- ko ifnot: $root.showText() -->
                            Show Text
                        <!-- /ko -->
                    </a>
                </td>
            </tr></table>
        </div>

        <div id="rightMenu" class="inactive" data-bind="
            css: {inactive: !$root.rightMenuIsOpened()},
            visible: $root.authorized
        ">
            <div class="closePanel" data-bind="click: $root.toggleRightMenu"></div>
            <div class="addedWrapper">
                <div class="closeBtn" data-bind="
                    click: $root.toggleRightMenu">
                </div>
                <!-- ko foreach: $root.rightPanelSentences -->
                    <div class="sentence" data-bind="
                        text: $data.text,
                        click: $root.rightPanelPlay($data.filename)
                    "></div>
                    <audio data-bind="attr:{
                        src: $data.filepath,
                        id: $data.filename
                    }"></audio>
                <!-- /ko -->
            </div>
        </div>
        <script src="app/js/app.js"></script>
    </body>
</html>

